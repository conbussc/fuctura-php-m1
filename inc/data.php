<?php

class Post
{
  public $titulo;
  public $texto;

  public function __construct($arr)
  {
    $this->titulo = $arr[0];
    $this->texto = $arr[1];
  }
}

$posts = [];
for ($i=0; $i < 5; $i++) {
  $posts[$i] = new Post(array('Titulo '.($i + 1), 'Texto '.($i + 1)));
}
