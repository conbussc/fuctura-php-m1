<div class="wrapper row1">
  <header id="header" class="clear">
    <div id="hgroup">
      <h1><a href="#"><?php echo $titulo; ?></a></h1>
      <h2><?php echo $subtitulo; ?></h2>
    </div>
    <nav>
      <ul>
        <?php
        foreach ($links_menu as $key => $value) {
          if($key == 5) {
            echo "<li class=\"last\"><a href=\"?pagina=$key\">$value</a></li>";
          } else {
            echo "<li><a href=\"?pagina=$key\">$value</a></li>";
          }
        }
        ?>
      </ul>
    </nav>
  </header>
</div>
