<?php

/**
* Neste arquivo serão declaradas todas as variáveis que serão
* exibidas na nossa página principal.
*/

$titulo = "Curso de PHP";
$subtitulo = "Introdução e Prática";

$links_menu = [];
$links_menu[1] = "Inicial";
$links_menu[2] = "Sobre o PHP";
$links_menu[3] = "Conteúdo";
$links_menu[4] = "Professores";
$links_menu[5] = "Contato";

$includes = [];
$includes[1] = 'inc/paginas/inicial.php';
$includes[2] = 'inc/paginas/sobre.php';
$includes[3] = 'inc/paginas/conteudo.php';
$includes[4] = 'inc/paginas/professores.php';
$includes[5] = 'inc/paginas/contato.php';

$date = date('Y');
$rodape = "Copyright © ${date} - Todos os direitos reservados - fuctura.com.br";

$title = "Curso de PHP";
$fuctura = "http://fuctura.com.br";

$categorias = $pdo->query('SELECT * FROM categorias');
