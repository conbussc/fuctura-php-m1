<!-- content -->
<div class="wrapper row2">
  <div id="container" class="clear">
    <!-- Slider -->
    <section id="slider"><a href="#"><img src="images/demo/960x360.gif" alt=""></a></section>
    <!-- main content -->
    <div id="homepage">
      <!-- services area -->
      <section id="services" class="clear">
        <article class="one_quarter">
          <figure><img src="images/demo/32x32.gif" width="32" height="32" alt=""></figure>
          <strong>Lorum ipsum dolor</strong>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non diam erat. In fringilla massa ut nisi ullamcorper.</p>
          <p class="more"><a href="#">Read More &raquo;</a></p>
        </article>
        <article class="one_quarter">
          <figure><img src="images/demo/32x32.gif" width="32" height="32" alt=""></figure>
          <strong>Lorum ipsum dolor</strong>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non diam erat. In fringilla massa ut nisi ullamcorper.</p>
          <p class="more"><a href="#">Read More &raquo;</a></p>
        </article>
        <article class="one_quarter">
          <figure><img src="images/demo/32x32.gif" width="32" height="32" alt=""></figure>
          <strong>Lorum ipsum dolor</strong>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non diam erat. In fringilla massa ut nisi ullamcorper.</p>
          <p class="more"><a href="#">Read More &raquo;</a></p>
        </article>
        <article class="one_quarter lastbox">
          <figure><img src="images/demo/32x32.gif" width="32" height="32" alt=""></figure>
          <strong>Lorum ipsum dolor</strong>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non diam erat. In fringilla massa ut nisi ullamcorper.</p>
          <p class="more"><a href="#">Read More &raquo;</a></p>
        </article>
      </section>
      <!-- / services area -->
      <!-- ########################################################################################## -->
      <!-- ########################################################################################## -->
      <!-- ########################################################################################## -->
      <!-- ########################################################################################## -->
      <!-- / latest work -->
      <section id="latest">
        <article>
          <figure>
            <ul class="clear">
              <li class="one_third"><img src="images/demo/290x180.gif" width="290" height="180" alt=""></li>
              <li class="one_third"><img src="images/demo/290x180.gif" width="290" height="180" alt=""></li>
              <li class="one_third lastbox"><img src="images/demo/290x180.gif" width="290" height="180" alt=""></li>
            </ul>
            <figcaption><a href="#">View All Our Recent Work Here &raquo;</a></figcaption>
          </figure>
        </article>
      </section>
      <!-- / latest work -->
    </div>
    <!-- main content -->
    <div id="content">
      <section id="posts" class="last clear">
        <ul>

          <?php for ($i=0;$i < 2;$i++): ?>
            <?php if ($i == 1): ?>
              <li class="last">
            <?php else: ?>
                <li>
            <?php endif; ?>
            <article class="clear">
              <figure><img src="images/demo/180x150.gif" alt="">
                <figcaption>
                  <h2><?php echo $posts[$i]->titulo; ?></h2>
                  <p><?php echo $posts[$i]->texto; ?></p>
                  <footer class="more"><a href="#">Read More &raquo;</a></footer>
                </figcaption>
              </figure>
            </article>
          </li>
        <?php endfor; ?>
        </ul>
      </section>
    </div>
    <!-- right column -->
    <aside id="right_column">
      <h2 class="title">Categories</h2>
      <nav>
        <ul>
          <?php foreach ($categorias as $categoria): ?>
          <li><a href="#"><?php echo $categoria['nome']; ?></a></li>
          <?php endforeach; ?>
        </ul>
      </nav>
      <!-- /nav -->
    </aside>
    <!-- / content body -->
  </div>
</div>
