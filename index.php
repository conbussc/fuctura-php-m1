<?php $pdo = new PDO('mysql:host=localhost;dbname=fucturaphp', 'root', 'root'); ?>
<?php require('inc/_variables.php'); ?>
<?php require('inc/_functions.php'); ?>
<?php require('inc/data.php'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title><?php echo $title; ?></title>
<meta charset="iso-8859-1">
<link rel="stylesheet" href="styles/layout.css" type="text/css">
<!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
</head>
<body>
<?php include('inc/header.php') ?>
<?php

if (isset($_GET['pagina'])) {
  $pagina = intval($_GET['pagina']);

  if ($pagina >= 1 && $pagina <= 5) {
    include($includes[$pagina]);
  } else {
    include('inc/404.php');
  }
} else {
  include($includes[1]);
}


?>

<?php include('inc/footer.php'); ?>
</body>
</html>
